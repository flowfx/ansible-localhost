#+TITLE: Doom Emacs Configuration
#+AUTHOR: Florian Posdziech
#+TOC: true

* Prerequisites
** Base system
✅ Install Ubuntu on new machine.

Install git, vim, and ansible.

#+begin_src shell
sudo apt install git neovim ansible
#+end_src

** Set up Git forges

Generate an SSH key.

#+begin_src shell
ssh-keygen -o -a 100 -t ed25519 -C "flowfx@hostname"
#+end_src

+ Upload it to GitHub
+ Upload it to Codeberg
+ ssh to `git@codeberg.org` once to add server to known hosts

** Bootstrap ansible playbook

Clone the repository.

#+begin_src shell
mkdir ~/src/flowfx
git clone git@github.com:FlowFX/ansible-localhost.git ~/src/flowfx/ansible-localhost
#+end_src

Cautiously run the bootstrap task

#+begin_src shell
cd ~/src/flowfx/ansible-localhost
ansible-playbook local.yml --tags bootstrap
#+end_src

* Go from there

For a new machine, run the playbook tag by tag.

#+begin_src shell
ansible-playbook local.yml --tags <tag>
#+end_src

Somewhere in there, we need to run some manual commands

+ Todo: configure onedrive & calendar
+ Run doom install
+ Run rcup (and disable the notifier in the playbook)
+ Run =asdf install=
+ Install Vim.plug https://github.com/junegunn/vim-plug#unix-linux
+ Install Inconsolata Nerd Font - Download from https://www.nerdfonts.com/ and
  unzip files to =~/.local/share/fonts=.
+ Run `emacs` on the command-line once - installs emojis

+ Clone triebwerk repository (+ docker-stacks)

  GIT_SSH_COMMAND='ssh -i private_key_file -o IdentitiesOnly=yes' git clone user@host:repo.git

 Add sshCommand = ssh -i ~/.ssh/id_ioki_ed25519   to .git/config

Install hex for elixir

$ mix local.hex

** More work foo

$ sudo apt install libcurl4-openssl-dev

required for `curb`

Install MS Teams

https://docs.microsoft.com/de-de/microsoftteams/get-clients#install-manually-from-the-command-line

Install fonts

install imagemagick!

install ctags !!

$ ansible-galaxy collection install ansible.posix
$ ansible-galaxy collection install community.general.
$ brew install ranger file chardet

For flowfx.de / Nikola
apt install yui-compressor

# Misc

$ apt install inotify-tools

$ texlive / install --cask mactex


** GitHub CLI
github cli
https://github.com/cli/cli/blob/trunk/docs/install_linux.md
curl -fsSL https://cli.github.com/packages/githubcli-archive-keyring.gpg | sudo gpg --dearmor -o /usr/share/keyrings/githubcli-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/githubcli-archive-keyring.gpg] https://cli.github.com/packages stable main" | sudo tee /etc/apt/sources.list.d/github-cli.list > /dev/null

sudo apt update
sudo apt install gh

Configure autocomplete!
https://cli.github.com/manual/gh_completion
